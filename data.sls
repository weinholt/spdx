;; SPDX-License-Identifier: MIT

#!r6rs

(library (spdx data)
  (export *licenses* *exceptions*)
  (import (rnrs) (srfi private include))
  (include/resolve () "data.scm"))
