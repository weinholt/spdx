#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

(import
  (rnrs (6))
  (srfi :64 testing)
  (spdx parser))

(test-begin "spdx-parser")
(test-equal '(or "LGPL-2.1" "MIT")
            (parse-license-expression "(LGPL-2.1 OR MIT)"))
(test-equal '(or "LGPL-2.1" (or "MIT" "BSD-3-Clause"))
            (parse-license-expression "(LGPL-2.1 OR MIT OR BSD-3-Clause)"))
(test-equal '(and "LGPL-2.1" "MIT")
            (parse-license-expression "(LGPL-2.1 AND MIT)"))
(test-equal '(and "LGPL-2.1" (and "MIT" "BSD-2-Clause"))
            (parse-license-expression "(LGPL-2.1 AND MIT AND BSD-2-Clause)"))
(test-equal '(with (+ "GPL-2.0") "Bison-exception-2.2")
            (parse-license-expression "(GPL-2.0+ WITH Bison-exception-2.2)"))
(test-end)

(test-begin "spdx-user")
(test-equal '(user-defined #f "23")
            (parse-license-expression "LicenseRef-23"))
(test-equal '(user-defined #f "MIT-Style-1")
            (parse-license-expression "LicenseRef-MIT-Style-1"))
(test-equal '(user-defined "spdx-tool-1.2" "MIT-Style-2")
            (parse-license-expression "DocumentRef-spdx-tool-1.2:LicenseRef-MIT-Style-2"))
(test-end)

(test-begin "spdx-precedence")
(test-equal '(or "LGPL-2.1" (and "BSD-3-Clause" "MIT"))
            (parse-license-expression "LGPL-2.1 OR BSD-3-Clause AND MIT"))
(test-equal '(and "MIT" (or (+ "LGPL-2.1") "BSD-3-Clause"))
            (parse-license-expression "(MIT AND (LGPL-2.1+ OR BSD-3-Clause))"))
(test-equal '(or (and "LGPL-2.1" "MIT") (and "BSD-3-Clause" "MIT"))
            (parse-license-expression "LGPL-2.1 AND MIT OR BSD-3-Clause AND MIT"))
(test-end)

(test-begin "spdx-ci")
;; The FAQ says expressions are case insensitive:
;; https://wiki.spdx.org/view/LicenseExpressionFAQ
(test-equal '(or (and (with (+ "GPL-2.0") "Bison-exception-2.2")
                      "MIT")
                 (user-defined "spdx-tool-1.2" "MIT-Style-2"))
            (parse-license-expression "GPL-2.0+ With Bison-exception-2.2 and MIT or documentref-spdx-tool-1.2:licenseref-MIT-Style-2"))
(test-end)

(test-begin "spdx-format")
(test-equal "(LGPL-2.1 OR MIT)"
            (format-license-expression
             (parse-license-expression "(LGPL-2.1 OR MIT)")))
(test-equal "(LGPL-2.1 OR MIT OR BSD-3-Clause)"
            (format-license-expression
             (parse-license-expression "(LGPL-2.1 OR MIT OR BSD-3-Clause)")))
(test-equal "(LGPL-2.1 AND MIT)"
            (format-license-expression
             (parse-license-expression "(LGPL-2.1 AND MIT)")))
(test-equal "(LGPL-2.1 AND MIT AND BSD-2-Clause)"
            (format-license-expression
             (parse-license-expression "(LGPL-2.1 AND MIT AND BSD-2-Clause)")))
(test-equal "(GPL-2.0+ WITH Bison-exception-2.2)"
            (format-license-expression
             (parse-license-expression "(GPL-2.0+ WITH Bison-exception-2.2)")))
(test-equal "(((GPL-2.0+ WITH Bison-exception-2.2) AND MIT) OR DocumentRef-spdx-tool-1.2:LicenseRef-MIT-Style-2)"
            (format-license-expression
             (parse-license-expression "GPL-2.0+ With Bison-exception-2.2 anD MIT oR documentref-spdx-tool-1.2:licenseref-MIT-Style-2")))
(test-equal "((LGPL-2.1 AND MIT) OR (BSD-3-Clause AND MIT))"
            (format-license-expression
             '(or (and "LGPL-2.1" "MIT") (and "BSD-3-Clause" "MIT"))))
(test-equal "LicenseRef-23"
            (format-license-expression (parse-license-expression "LicenseRef-23")))
(test-end)

(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
