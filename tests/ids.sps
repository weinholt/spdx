#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

(import
  (rnrs (6))
  (srfi :64 testing)
  (spdx ids))

(test-begin "ids-lookup")
(test-assert (for-all license-exception (license-exceptions)))
(test-assert (for-all license (licenses)))
(test-end)

(test-begin "ids-lookup-ci")
(test-assert (for-all (lambda (x) (license-exception (string-downcase x)))
                      (license-exceptions)))
(test-assert (for-all (lambda (x) (license (string-downcase x)))
                      (licenses)))
(test-end)


(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
