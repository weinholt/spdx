# SPDX license expressions in R6RS Scheme

[SPDX][spdx] license expressions are a modern way to tag source code
and binaries with their license. They are used in the Linux kernel,
package managers and many other places.

  [spdx]: https://spdx.org/

## Parser API

```Scheme
(import (spdx parser))
```

### (parse-license-expression string)

Parses *string* as a license expression, returning it as an
S-expression:

 - (**or** *expr0* *expr1*) — A choice between license expressions.
 - (**and** *expr0* *expr1*) — A requirement to comply with both
   license expressions.
 - (**with** *license* *exception*) — A license identifier with a
   license exception identifier (both strings).
 - (**user-defined** *document-ref* *license-ref*) — This is a
   user-defined license reference. The document is optional and `#f`
   if omitted, otherwise both are strings.
 - (**+** *license-identifier*) — Represents "(or later)". Example:
   `(+ "GPL-3.0")` represents "GNU GPL version 3 (or later)".
 - *license-identifier* — A string identifying a license. Example:
   `"GPL-3.0"`.

Allowed characters in strings are A-Z, a-z, 0-9 and hyphen (-). No
check is done to verify that license identifiers and license exception
identifiers are valid.

```Scheme
> (parse-license-expression "(GPL-2.0+ WITH Bison-exception-2.2)")
(with (+ "GPL-2.0") "Bison-exception-2.2")
```

### (format-license-expression expr)

Returns a string representation of *expr*.

## Identifier API

```Scheme
(import (spdx ids))
```

### (identifier-list-version)

Returns the version of the SPDX license list contained in the library,
e.g. `"3.0"`.

### (licenses)

Returns a list of license identifiers.

### (license *string*)

Returns information for a license if it exists, otherwise `#f`. The
string is case-insensitive.

The data is an alist that is guaranteed to contain the symbols `id`
and `name`. Other symbols are the booleans `osi-approved?`,
`fsf-libre?` and `deprecated?`. More items may be added in the future.

```Scheme
> (license "mit")
=> ((id . "MIT")
    (name . "MIT License")
    (fsf-libre? . #t)
    (osi-approved? . #t)
    (deprecated? . #f))
```

### (license-exceptions)

Returns a list of license exception identifiers.

### (license-exception *string*)

Returns information for a license exception if it exists, otherwise
`#f`.

The data is an alist that is guaranteed to contain the symbols `id`
and `name`.

```Scheme
> (license-exception "CLISP-exception-2.0")
=> ((id . "CLISP-exception-2.0")
    (name . "CLISP exception 2.0"))
```

## License

SPDX-License-Identifier: MIT

Copyright © 2018 Göran Weinholt

See [LICENSE](LICENSE).
