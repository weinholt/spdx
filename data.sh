#!/bin/sh
set -eu
cd "$(dirname "$0")"
echo "Entering directory '$PWD'"
set -x
curl --location --fail --silent --show-error -o data-licenses.json \
     https://raw.githubusercontent.com/spdx/license-list-data/master/json/licenses.json
curl --location --fail --silent --show-error -o data-exceptions.json \
     https://raw.githubusercontent.com/spdx/license-list-data/master/json/exceptions.json
gosh data-generator.scm
