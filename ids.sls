;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;; The tables are based on the SPDX license list at
;; https://github.com/spdx/license-list-data

(library (spdx ids)
  (export
    licenses
    license
    license-exceptions
    license-exception)
  (import
    (rnrs)
    (only (srfi :43 vectors) vector-binary-search)
    (srfi :67 compare-procedures)
    (spdx data))

(define (licenses)
  (vector->list (vector-map car *licenses*)))

(define (license name)
  (assert (string? name))
  (let ((idx (vector-binary-search *licenses* name
                                   (lambda (v n)
                                     (string-compare-ci (car v) n)))))
    (and idx
         (let ((entry (vector-ref *licenses* idx)))
           (cons (cons 'id (car entry)) (cdr entry))))))

(define (license-exceptions)
  (vector->list (vector-map car *exceptions*)))

(define (license-exception name)
  (assert (string? name))
  (let ((idx (vector-binary-search *exceptions* name
                                   (lambda (v n)
                                     (string-compare-ci (car v) n)))))
    (and idx
         (let ((entry (vector-ref *exceptions* idx)))
           (cons (cons 'id (car entry)) (cdr entry))))))

(define (identifier-list-version)
  "3.0")
)
